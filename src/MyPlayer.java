import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer extends Player {

    Random rand;
    private int moveCounter;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        moveCounter = 0;
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }



    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();

        //puts it in middle spot if first move
        if (moveCounter == 0) {
            moveCounter++;
            return 3;
        }
        else {
            Move bestMove = search(gameBoard, 5, this.playerNumber);
            System.out.println(bestMove.value);

            long diff = System.nanoTime() - start;
            double elapsed = (double) diff / 1e9;
            System.out.println("Elapsed Time: " + elapsed + " sec");
            moveCounter++;
            return bestMove.move;
        }
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber) {

        ArrayList<Move> moves = new ArrayList<Move>();

        // Try each possible move
        for (int i=0; i<Board.BOARD_SIZE; i++) {

            // Skip this move if the column isn't open
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }

            // Place a tile in column i
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            // Check to see if that ended the game
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {

                if (gameStatus == 0) {
                    // Tie game
                    thisMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                    // Win
                    thisMove.value = 1.0;
                } else {
                    // Loss
                    thisMove.value = -1.0;
                }

            } else if (maxDepth == 0) {
                // If we can't search any more levels down then apply a heuristic to the board
                thisMove.value = heuristic(gameBoard, playerNumber);

            } else {
                // Search down an additional level
                Move responseMove = search(gameBoard, maxDepth-1, (playerNumber == 1 ? 2 : 1));
                thisMove.value = -responseMove.value;
            }

            // Store the move
            moves.add(thisMove);

            // Remove the tile from column i
            gameBoard.undoMove(i);
        }

        // Pick the highest value move
        return this.getBestMove(moves);

    }

    public double heuristic(Board gameBoard, int playerNumber)
    {
        double total;
        double baseCaseTotal;
        total = twoInARow(gameBoard, playerNumber);
        baseCaseTotal = baseCase(gameBoard, playerNumber);
       // System.out.println("total " + Math.max(total, baseCaseTotal));
        return Math.max(total,baseCaseTotal);

    }

    private double baseCase(Board gameBoard, int playerNumber)
    {
        double total = 0.0;
        int[][] arr = gameBoard.getBoard();
        int otherPlayer=0;
        if (playerNumber==1)
        {
            otherPlayer=2;
        }
        else if(playerNumber==2)
        {
            otherPlayer=1;
        }
        for (int col = 0; col < 5; col++)
        {
            for (int row = 0; row < 5; row++) {

                //if there is a piece in this position
                if(arr[row][col]==playerNumber) {
                    //horizontal
                    if (arr[row + 1][col] ==0)
                    {
                        total =.6;
                    }
                    if(arr[row+1][col]==otherPlayer|| arr[row+1][col]==playerNumber)
                    {
                        total=.6;
                    }
                    else
                    {
                        if(arr[row][col+1]==0)
                        {
                            total=.8;
                        }
                    }

                }
            }
        }

        return total;
    }



    //If not first move, blocks two in a row
    private double twoInARow(Board gameBoard, int playerNumber) {
        double total = 0.0;
        int[][] arr = gameBoard.getBoard();

        int otherPlayer=0;
        if (playerNumber==1)
        {
            otherPlayer=2;
        }
        else
        {
            otherPlayer=1;
        }

        for (int row = 5; row > 0; row--) {
            for (int col = 0; col < 5; col++) {

                /*
                I think we have too many and its getting confused but its not working
                 */
                //for two in a row
                //for vertical
                if ((row <= 3) && (arr[row][col] != otherPlayer) && (((arr[row + 1][col] == otherPlayer)&& (arr[row+2][col] == 0))|| ((arr[row+1][col]==playerNumber) && (arr[row+2][col] == 0))))
                {
                  // if ((arr[row-1][col]==playerNumber||(arr[row-1][col] == otherPlayer) && ((arr[row-2][col] ==playerNumber) || arr[row-2][col] == otherPlayer)))
                        total=.8;
                }
                //horizontal
                if ((col <= 3) && (arr[row][col] == otherPlayer) && (((arr[row][col + 1]  == otherPlayer) && (arr[row][col+2] ==0))|| ((arr[row+1][col]==playerNumber) && (arr[row][col+2] ==0)))) {
                    total = .8;
                }
                //horizontal greater than 3
                if ((col >= 3) && (arr[row][col]  == otherPlayer) && (((arr[row][col - 1]  == otherPlayer) && (arr[row][col-2] == 0)) || ((arr[row][col-1]==playerNumber)&& (arr[row][col-2] == 0)))) {

                    if (row>=3 && arr[row-1][col] !=0 && arr[row-2][col]!=0){
                        total =.8;
                    }
                    else {
                        total = .2;
                    }
                }
                //for two in a row
                //for vertical
                if ((row <= 3) && (arr[row][col]  == otherPlayer) && (((arr[row + 1][col]  == otherPlayer) && (arr[row+2][col] == 1))|| (arr[row+1][col]==playerNumber) && (arr[row+2][col] == 1))) {
                    total = 0.4;
                }
                //horizontal
                if ((col <= 3) && (arr[row][col]  == otherPlayer) && (((arr[row][col + 1]  == otherPlayer)&& (arr[row][col+2] == 1))|| (arr[row][col+1]==playerNumber) && (arr[row][col+2] == 1))) {
                    total = 0.4;
                }
                if ((col >= 3) && (arr[row][col]  == otherPlayer) && (((arr[row][col - 1]  == otherPlayer)&& ((arr[row][col-2] == 1)) || (arr[row][col-1]==playerNumber) && (arr[row][col-2] == 1)))) {
                    total = 0.4;
                }
            }
        }
        return total;
    }



    private Move getBestMove(ArrayList<Move> moves) {

        double max = -1.0;
        Move bestMove = moves.get(0);

        for (Move cm : moves) {
            if (cm.value > max) {
                max = cm.value;
                bestMove = cm;
            }
        }
        return bestMove;
    }
}
