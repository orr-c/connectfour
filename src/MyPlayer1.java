import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer1 extends Player {

    Random rand;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer1() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }


    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();

        Move bestMove = search(gameBoard, 6, this.playerNumber);
        System.out.println(bestMove.value);

        long diff = System.nanoTime()-start;
        double elapsed = (double)diff/1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        return bestMove.move;
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber) {

        ArrayList<Move> moves = new ArrayList<Move>();

        // Try each possible move
        for (int i=0; i<Board.BOARD_SIZE; i++) {

            // Skip this move if the column isn't open
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }

            // Place a tile in column i
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            // Check to see if that ended the game
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {

                if (gameStatus == 0) {
                    // Tie game
                    thisMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                    // Win
                    thisMove.value = 1.0;
                } else {
                    // Loss
                    thisMove.value = -1.0;
                }

            } else if (maxDepth == 0) {
                // If we can't search any more levels down then apply a heuristic to the board
                thisMove.value = heuristic(gameBoard, playerNumber);

            } else {
                // Search down an additional level
                Move responseMove = search(gameBoard, maxDepth-1, (playerNumber == 1 ? 2 : 1));
                thisMove.value = -responseMove.value;
            }

            // Store the move
            moves.add(thisMove);

            // Remove the tile from column i
            gameBoard.undoMove(i);
        }

        // Pick the highest value move
        return this.getBestMove(moves);

    }

    public double heuristic(Board gameBoard, int playerNumber) {
        // This should return a number between -1.0 and 1.0.
        //
        // If the board favors playerNumber then the return value should be close to 1.0
        // If the board favors playerNumber's opponent then the return value should be close to 1.0
        // If the board favors neither player then the return value should be close to 0.0

        return 0.0;
    }


    private Move getBestMove(ArrayList<Move> moves) {

        double max = -1.0;
        Move bestMove = moves.get(0);

        for (Move cm : moves) {
            if (cm.value > max) {
                max = cm.value;
                bestMove = cm;
            }
        }

        return bestMove;
    }



}